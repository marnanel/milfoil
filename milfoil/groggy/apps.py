from django.apps import AppConfig


class GroggyConfig(AppConfig):
    name = 'groggy'
