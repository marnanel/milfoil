from milfoil.groggy.tests import GroggyTestCase
import milfoil.groggy.models as groggy_models
from django.contrib.auth.models import User
from django.conf import settings

class Tests(GroggyTestCase):

    def test_simple(self):

        user = User(username='fred')
        user.save()

        item = groggy_models.Item()
        item.save()

        item.append(
                poster = user,
                grogname = 'wombat',
                text = """I like bananas
Monkey nuts
And grapes""")
       
        self.assertEqual(
                item.entries[0].content,
                """I like bananas
Monkey nuts
And grapes""")

    def test_multi_entries(self):
        wombat = User(username='wombat')
        wombat.save()

        fred = User(username='fred')
        fred.save()

        jane = User(username='jane')
        jane.save()

        item = groggy_models.Item()
        item.save()

        item.append(
                poster = wombat,
                grogname = 'wombat',
                text = """I like bananas
Monkey nuts
And grapes""")

        item.append(
                poster = fred,
                grogname = 'fred',
                text = "Gosh, so do I")

        item.append(
                poster = jane,
                grogname = 'jane',
                text = "Who knew?")

        self.assertEqual(
                len(item.entries),
                3)
       
        self.assertEqual(
                item.count(),
                3)

        self.assertEqual(
                item.entries[2].grogname,
                'jane')

        self.assertEqual(
                item.entries[2].content,
                "Who knew?")

    def test_continuation(self):
        wombat = User(username='wombat')
        wombat.save()

        first_item = groggy_models.Item()
        first_item.save()

        items = [first_item]
        spill_point = None

        for i in range(20):

            try:

                items[-1].append(
                        poster = wombat,
                        grogname = 'wombat',
                        text = f"Entry number {i}\n\nThis is entry number {i}")

            except groggy_models.Item.FullException:

                self.assertEqual(i % settings.MILFOIL_MAX_ITEM_LENGTH, 0)
                spill_point = i
                items.append(items[-1].continuation())

        self.assertIsNotNone(spill_point)
        self.assertNotEqual(
                first_item,
                items[-1],
                )
